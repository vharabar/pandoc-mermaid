#  Lorem ipsum dolor sit amet,

## consectetur adipiscing elit. 

Donec faucibus facilisis arcu ac cursus. Maecenas ullamcorper luctus metus, quis tempus urna vehicula sed. Etiam tincidunt volutpat nisi, a porta turpis aliquam interdum. Quisque ut lorem et tortor hendrerit lacinia at vel ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nunc ac rhoncus arcu. Donec ornare felis eget est eleifend, sed pretium felis bibendum. Vestibulum arcu ipsum, placerat at interdum at, posuere et nunc. Vivamus sollicitudin neque mauris, id dignissim purus condimentum eget. Proin in tempor erat. Duis quam ligula, pulvinar in pretium non, interdum et nunc. In elementum tempus tortor dapibus imperdiet. `Praesent cursus vestibulum pellentesque`.  
[This is a test link](www.example.com)

 * Phasellus et lacus odio. 
 * Nam rhoncus elit erat, at molestie arcu gravida ut. 
 * Morbi mauris risus, luctus in porta eu, iaculis eu est. 

```mermaid
sequenceDiagram
    participant Alice
    participant Bob
    Alice->>John: Hello John, how are you?
    loop Healthcheck
        John->>John: Fight against hypochondria
    end
    Note right of John: Rational thoughts <br/>prevail!
    John-->>Alice: Great!
    John->>Bob: How about you?
    Bob-->>John: Jolly good!
```

### Donec convallis at felis luctus tempor. Phasellus mattis, sem et mattis elementum, elit velit mattis purus, pulvinar rhoncus sem nulla vel augue.  

Pellentesque consectetur, eros vel imperdiet accumsan, nisl turpis cursus arcu, ut tincidunt ipsum lectus et enim. Maecenas congue justo ut laoreet porta. Nam vel arcu sapien. Proin viverra placerat leo eget viverra. Aliquam eget nibh tincidunt, pharetra leo vel, interdum mauris.  
> Curabitur `ullamcorper augue` eu enim mattis, ac imperdiet massa ultricies. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc cursus auctor nibh at sagittis. Sed nec eros leo. Morbi rutrum, nisl sit amet egestas posuere, ligula sem sagittis libero, nec dignissim massa velit tempus felis. Nulla facilisi. In molestie faucibus quam vel pulvinar. 

```mermaid
gantt
dateFormat  YYYY-MM-DD
title Adding GANTT diagram to mermaid
excludes weekdays 2014-01-10

section A section
Completed task            :done,    des1, 2014-01-06,2014-01-08
Active task               :active,  des2, 2014-01-09, 3d
Future task               :         des3, after des2, 5d
Future task2               :         des4, after des3, 5d
```

Nunc nec justo porttitor, feugiat nisl et, ullamcorper enim. Nam et viverra nibh, et malesuada nisl. Aenean semper, felis sed lobortis laoreet, ipsum eros ornare ligula, ac pretium augue ex et turpis.  

 1. Sed venenatis a risus sit amet maximus. 
 2. Aliquam ante nulla, molestie eget erat sed, scelerisque scelerisque magna. 
 3. Mauris eu scelerisque ante.  
   
Proin venenatis, leo eget scelerisque consequat, nisl neque ultrices nibh, et rutrum dui mi sit amet ante. 

```mermaid
gitGraph:
options
{
    "nodeSpacing": 150,
    "nodeRadius": 10
}
end
commit
branch newbranch
checkout newbranch
commit
commit
checkout master
commit
commit
merge newbranch
```

Vestibulum quam libero, sagittis eget sem et, elementum aliquam orci. Duis dui justo, lacinia quis leo quis, laoreet lacinia risus. Integer bibendum lorem ac massa vulputate ultricies. Maecenas nibh purus, ultrices at viverra facilisis, convallis ut nibh. 

Nullam quis libero vitae ipsum sodales pretium ut vitae velit. Integer mauris velit, finibus pretium tincidunt vitae, luctus id turpis. 
[this is another test link][1]
```
Nulla vitae molestie urna. Curabitur mauris enim, vestibulum sed maximus in, porta sit amet magna. Aliquam luctus felis malesuada risus lobortis ultricies. Quisque libero mi, varius vitae tellus fermentum, vestibulum vulputate tortor. Fusce eget orci ut velit malesuada eleifend eu ac urna. 
```
ras dictum et arcu in auctor. Nunc malesuada, sapien vel tincidunt aliquet, nulla mauris consectetur augue, ut ullamcorper lacus risus sit amet massa. Vivamus augue ligula, sagittis nec mollis et, mattis non eros. 

[1]: http://google.com