FROM ubuntu:rolling

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get upgrade
RUN apt install software-properties-common -y
RUN add-apt-repository ppa:saiarcot895/chromium-beta -y
RUN apt-get update -y
RUN apt-get install nodejs npm pandoc -y
RUN apt-get install texlive-latex-extra librsvg2-bin -y
RUN apt-get install chromium-browser -y
RUN mkdir /mermaid && cd /mermaid
WORKDIR /mermaid

RUN npm init -y -f
RUN npm i @mermaid-js/mermaid-cli mermaid-filter katex

ENV PATH=$PATH:/mermaid/node_modules/.bin/

RUN mkdir /work && cd /work
WORKDIR /work

COPY ./puppeteer-config.json /puppeteer-config.json

RUN useradd -u 1234 pandoc
USER pandoc

ENTRYPOINT [ "/bin/bash" ]
